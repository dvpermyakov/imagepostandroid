package com.dvpermyakov.imagepostapplication.views

/**
 * Created by dmitrypermyakov on 30/04/2018.
 */

interface PermissionView {
    fun showPreviousScreen()
}