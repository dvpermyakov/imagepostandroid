package com.dvpermyakov.imagepostapplication.utils

/**
 * Created by dmitrypermyakov on 01/05/2018.
 */

object ImagePostApplicationConstants {
    const val INTENT_EXTRA_STICKER_MODEL = "intent_extra_sticker_model"
}